import { environment } from "src/environments/environment"
export const userAuthenticate = `${environment.apiUrl}/login`;
export const holiday = `${environment.apiUrl}/holiday`;
export const users = `${environment.apiUrl}/users/getAll`;
export const report = `${environment.apiUrl}/report/`
