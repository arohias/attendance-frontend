import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UserComponent } from './user/user.component';
import { UserModule } from './user/user.module';
import { HeaderComponent } from './header/header.component';


@NgModule({
  declarations: [
    DashboardComponent,
    HeaderComponent,
    // UserComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    UserModule
  ]
})
export class DashboardModule { }
