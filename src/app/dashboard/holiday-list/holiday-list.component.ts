import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HolidayService } from 'src/app/services/holiday.services';

@Component({
  selector: 'app-holiday-list',
  templateUrl: './holiday-list.component.html',
  styleUrls: ['./holiday-list.component.scss']
})
export class HolidayListComponent implements OnInit {
  holidays: any[] = [];
  holidayForm!: FormGroup;
  isadd = false;
  constructor(private holidayService: HolidayService, private formBuilder: FormBuilder) {
    this.holidayForm = this.formBuilder.group({
      date: ['', [Validators.required, Validators.email]],
      purpose: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getHolidays();
  }

  getHolidays() {
    this.holidayService.getAll().then(res => {
      this.holidays = res.data;
    })
  }
  add() {
    this.isadd = true;
  }
  addHoliday() {
    const data = Object.assign(this.holidayForm.value);
    this.holidayService.addHoliday(data).then((res) => {
      this.getHolidays();
    })
  }
}
