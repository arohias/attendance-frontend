import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HolidayListRoutingModule } from './holiday-list-routing.module';
import { HolidayListComponent } from './holiday-list.component';


@NgModule({
  declarations: [
    HolidayListComponent
  ],
  imports: [
    CommonModule,
    HolidayListRoutingModule
  ]
})
export class HolidayListModule { }
