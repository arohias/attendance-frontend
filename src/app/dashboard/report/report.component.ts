import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  constructor(private reportService: ReportService) { }
  report: any
  ngOnInit(): void {
    this.getReport();
  }
  getReport() {
    this.reportService.getAll().then((res) => {
      this.report = res.data;
    })
  }
}
