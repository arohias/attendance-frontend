import { Component, OnInit } from '@angular/core';
import { users } from 'src/app/api';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users: any
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }
  getUsers() {
    this.userService.getUsers().then((res: any) => {
      console.log(res);
      this.users = res.data;
    })
  }
}
