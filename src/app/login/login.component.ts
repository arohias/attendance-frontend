import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  submitted = false;
  isLoading = false;
  errorMessage = '';
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService) { }
  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  get f() {
    return this.loginForm.controls;
  }
  login() {
    const data = Object.assign(this.loginForm.value);
    console.log(data, this.loginForm);

    this.submitted = true;
    this.isLoading = true;
    // if (this.loginForm.valid) {
    this.userService.login(data).then((res) => {
      this.router.navigate(['/dashboard/users']);
    }).catch((error) => {
      this.isLoading = false;
      if (error.status === 401 || error.status === 500) {
        this.router.navigate(['/login']);
      }
    })
    // }

  }
}
