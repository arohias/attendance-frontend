import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { holiday } from '../api';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class HolidayService {
  constructor(
    private http: HttpClient,
    private router: Router) {
  }

  getAll(): Promise<any> {
    return this.http.get(`${holiday}/getAll`).toPromise();
  }
  addHoliday(data: any) {
    return this.http.post(holiday, data).toPromise();
  }
}

