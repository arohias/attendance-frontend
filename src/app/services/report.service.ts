import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { report } from '../api';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(
    private http: HttpClient,
    private router: Router) {
  }

  getAll(): Promise<any> {
    return this.http.get(`${report}/MONTHLY/1`).toPromise();
  }
}

