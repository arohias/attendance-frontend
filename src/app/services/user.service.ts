import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userAuthenticate, users } from '../api';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private http: HttpClient,
    private router: Router) {
  }

  login(user: any): Promise<any> {
    return this.http.post(userAuthenticate, { email: user.email, password: user.password }).toPromise();
  }
  getUsers() {
    return this.http.get(users).toPromise();
  }
}
